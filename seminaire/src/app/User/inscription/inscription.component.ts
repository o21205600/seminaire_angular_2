import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormsModule } from '@angular/forms';
import { User } from '../user';
import { CommonModule } from '@angular/common';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { catchError, tap } from 'rxjs/operators';
@Component({
  selector: 'app-inscription',
  standalone: true,
  imports: [CommonModule,FormsModule ],
  templateUrl: './inscription.component.html',
  styleUrl: './inscription.component.scss'
})
export class InscriptionComponent {

  constructor(private userService: UserService) { }

  onSubmitForm(form: NgForm): void {
    if (form.valid) {
      const { nom, email, password } = form.value;
      this.userService.insertUser(nom, email, password)
        .pipe(
          tap(response => {
            console.log('Utilisateur inséré avec succès :', response);
            form.resetForm();
          }),
          catchError(error => {
            console.error('Erreur lors de l\'inscription :', error);
            throw error; // Re-lance l'erreur pour la gérer ultérieurement si nécessaire
          })
        )
        .subscribe();
    } else {
      console.error('Le formulaire n\'est pas valide');
    }
  }
}