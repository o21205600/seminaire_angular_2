import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule,Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'; // Import the map operator
import { Seminaire } from '../models/site_models';
import { CommonModule } from '@angular/common';
import { SeminaireService } from '../services/seminaire.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-seminaire',
  standalone: true,
  imports: [ReactiveFormsModule,CommonModule],
  templateUrl: './new-seminaire.component.html',
  styleUrl: './new-seminaire.component.scss'
})
export class NewSeminaireComponent implements OnInit {
  
  seminaireForm!: FormGroup;
  seminairePreview$!: Observable<Seminaire>;

  constructor(private formBuilder: FormBuilder,private seminaireSerice:SeminaireService,private router:Router) {}
  
  ngOnInit(): void {
    this.seminaireForm = this.formBuilder.group({
      titre: [null, Validators.required],
      date: [null, Validators.required],
      intervenant: [null, Validators.required],
      lieu: [null, Validators.required],
      resume: [null],
      domaine: [null]
    }, {
      updateOn: 'blur'  /*permet l ajout du champ lorsque celui-ci à été ajouté*/ 
    });

    this.seminairePreview$ = this.seminaireForm.valueChanges.pipe(
      map(formValue => {
        return {
          ...formValue,
          id:0
        }
      })
    );
  }

  onSubmitForm(): void {
    this.seminaireSerice.addSeminaire(this.seminaireForm.value);
    this.router.navigateByUrl('/seminaires');
  }
  
}
