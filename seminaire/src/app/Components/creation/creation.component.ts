import { Component, OnInit } from '@angular/core';
import { SeminaireService } from '../../services/seminaire.services';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@Component({
  selector: 'app-creation',
  standalone: true,
  imports: [ReactiveFormsModule ],
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent implements OnInit {
  seminaireForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private seminaireService: SeminaireService, private router: Router) {}

  ngOnInit(): void {
    this.seminaireForm = this.formBuilder.group({
      titre: [null, Validators.required],
      date: [null, Validators.required],
      intervenant: [null, Validators.required],
      lieu: [null, Validators.required],
      resume: [null],
      domaine: [null]
    });
  }

  onSubmitForm(): void {
    this.seminaireService.addSeminaire(this.seminaireForm.value);
    this.router.navigateByUrl('/seminaires');
  }
}
