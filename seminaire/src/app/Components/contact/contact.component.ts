import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  onSubmitForm(): void {
    // Logique de soumission du formulaire
    if (this.contactForm.valid) {
      console.log('Formulaire valide, soumission en cours...');
      console.log('Nom:', this.contactForm.value.name);
      console.log('Email:', this.contactForm.value.email);
      console.log('Message:', this.contactForm.value.message);
      // Vous pouvez envoyer les données à votre backend ici, par exemple
      // this.userService.submitContactForm(this.contactForm.value).subscribe(response => {
      //   console.log('Réponse du serveur:', response);
      // });
    } else {
      console.log('Formulaire invalide, veuillez vérifier les champs.');
    }
  }
}
