import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Seminaire } from '../../models/site_models';
import { CommonModule } from '@angular/common';
import { SiteComponent } from '../site/site_component';
import { HeaderComponent } from '../header/header.component';
import { SeminaireService } from '../../services/seminaire.services';
import { Router } from '@angular/router';
@Component({
  selector: 'app-seminaire-list',
  standalone: true,
  imports: [CommonModule,SiteComponent,HeaderComponent],
  templateUrl: './seminaire-list.component.html',
  styleUrl: './seminaire-list.component.scss'
})
export class SeminaireListComponent implements OnInit {
  seminaires!: Seminaire[];
  seminairesFiltres!: Seminaire[];


  constructor(private seminaireService:SeminaireService,private router:Router) {}

  ngOnInit(): void {
    this.seminaires =this.seminaireService.getAllSeminaire();
    this.seminairesFiltres = [];
    this.applyFiltersByTous(); // Appel de la fonction pour afficher tous les séminaires par défaut
    this.applyFiltersByAucun(); // Appel de la fonction pour afficher tous les séminaires par défaut
  }
  getSeminaires(): void {
    this.seminaires = this.seminaireService.getAllSeminaire();
  }
  applyFiltersByMath(): void {
    this.seminairesFiltres = this.seminaires.filter(seminaire => {
      return seminaire.domaine === 'Math';
    });
  }
  
  applyFiltersByPhysique(): void {
    this.seminairesFiltres = this.seminaires.filter(seminaire => {
      return seminaire.domaine === 'Physique';
    });
  }
  
  applyFiltersByInformatique(): void {
    this.seminairesFiltres = this.seminaires.filter(seminaire => {
      return seminaire.domaine === 'Informatique';
    });
  }
  
  applyFiltersByPhilosophie(): void {
    this.seminairesFiltres = this.seminaires.filter(seminaire => {
      return seminaire.domaine === 'Philosophie';
    });
  }
  applyFiltersByTous(): void {
    this.seminairesFiltres =this.seminaireService.getAllSeminaire();

  }
  applyFiltersByRecent(): void {
    this.seminairesFiltres = [...this.seminaires].sort((a, b) => b.date.getTime() - a.date.getTime());
  }
  
  applyFiltersByAncien(): void {
    this.seminairesFiltres = [...this.seminaires].sort((a, b) => a.date.getTime() - b.date.getTime());
  }
  applyFiltersByAucun(): void {
    this.seminairesFiltres =this.seminaireService.getAllSeminaire();
  }
  AddSeminaire(): void{
    this.router.navigateByUrl('/create');
  }
  
  SupSeminaire(): void {
    this.router.navigateByUrl('/seminaires')
    this.seminaireService.SupSeminaire();
  }
}
