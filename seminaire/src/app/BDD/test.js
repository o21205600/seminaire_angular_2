const db = require('./db');

// Insérer un nouvel utilisateur
db.insertUser('john_doe3', 'john@example.com3', 'password1233', (err, userId) => {
    if (err) {
        console.error('Erreur lors de l\'insertion de l\'utilisateur :', err);
    } else {
        console.log('Utilisateur inséré avec succès. ID :', userId);
        
        // Récupérer tous les utilisateurs
        db.getAllUsers((err, users) => {
            if (err) {
                console.error('Erreur lors de la récupération des utilisateurs :', err);
            } else {
                console.log('Utilisateurs récupérés avec succès :', users);
            }
        });
    }
});


// Insérer un nouveau séminaire
db.insertSeminaire(
    "Séminaire sur les nouvelles technologies",
    "2024-04-15",
    "Dr. Jean Dupont",
    "Amphithéâtre 101, Campus Saint-Charles, Marseille",
    "Exploration des avancées technologiques récentes",
    "Informatique",
    (err, seminaireId) => {
        if (err) {
            console.error('Erreur lors de l\'insertion du séminaire :', err);
        } else {
            console.log('Séminaire inséré avec succès. ID :', seminaireId);
        }
    }
);
