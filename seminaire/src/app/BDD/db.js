const sqlite3 = require('sqlite3').verbose();

// Ouvrir la base de données SQLite
const db = new sqlite3.Database('data2.db');

// Créer la table des utilisateurs si elle n'existe pas
db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT NOT NULL,
        email TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL
    )`);
});

// Fonction pour insérer un nouvel utilisateur dans la base de données
function insertUser(username, email, password, callback) {
    const insertUserQuery = 'INSERT INTO users (username, email, password) VALUES (?, ?, ?)';
    db.run(insertUserQuery, [username, email, password], function(err) {
        if (err) {
            callback(err);
        } else {
            callback(null, this.lastID); // Renvoie l'ID du nouvel utilisateur inséré
        }
    });
}

// Fonction pour récupérer tous les utilisateurs de la base de données
function getAllUsers(callback) {
    const selectUsersQuery = 'SELECT * FROM users';
    db.all(selectUsersQuery, function(err, rows) {
        if (err) {
            callback(err);
        } else {
            callback(null, rows);
        }
    });
}
function insertSeminaire(titre, date, intervenant, lieu, resume, domaine, callback) {
    const insertSeminaireQuery = 'INSERT INTO seminaires (titre, date, intervenant, lieu, resume, domaine) VALUES (?, ?, ?, ?, ?, ?)';
    db.run(insertSeminaireQuery, [titre, date, intervenant, lieu, resume, domaine], function(err) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, this.lastID);
        }
    });
}

module.exports = {
    insertUser,
    getAllUsers,
    insertSeminaire
};
