const express = require('express');
const sqlite3 = require('sqlite3').verbose();

const app = express();
const port = process.env.PORT || 3000;

// Ouvrir la base de données SQLite
const db = new sqlite3.Database('data2.db');

// Middleware pour parser le corps des requêtes en JSON
app.use(express.json());

// Créer la table des séminaires si elle n'existe pas
db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS seminaires (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        titre TEXT NOT NULL,
        date TEXT NOT NULL,
        intervenant TEXT NOT NULL,
        lieu TEXT NOT NULL,
        resume TEXT,
        domaine TEXT NOT NULL
    )`);
});

// Route pour récupérer tous les séminaires
app.get('/seminaires', (req, res) => {
    const selectSeminairesQuery = 'SELECT * FROM seminaires';
    db.all(selectSeminairesQuery, (err, rows) => {
        if (err) {
            console.error(err);
            res.status(500).json({ error: 'Erreur lors de la récupération des séminaires' });
        } else {
            res.status(200).json(rows);
        }
    });
});

// Route pour insérer un nouveau séminaire dans la base de données
app.post('/seminaires', (req, res) => {
    const { titre, date, intervenant, lieu, resume, domaine } = req.body;
    const insertSeminaireQuery = 'INSERT INTO seminaires (titre, date, intervenant, lieu, resume, domaine) VALUES (?, ?, ?, ?, ?, ?)';
    db.run(insertSeminaireQuery, [titre, date, intervenant, lieu, resume, domaine], function(err) {
        if (err) {
            console.error(err);
            res.status(500).json({ error: 'Erreur lors de l\'insertion du séminaire' });
        } else {
            console.log('Séminaire inséré avec succès. ID :', this.lastID);
            res.status(200).json({ message: 'Séminaire inséré avec succès' });
        }
    });
});

// Démarrer le serveur
app.listen(port, () => {
    console.log(`Serveur Node.js démarré sur le port ${port}`);
});
