import { Injectable, OnInit } from '@angular/core';
import { Seminaire } from '../models/site_models';

@Injectable({
  providedIn: 'root'
})
export class SeminaireService  {
  seminaires:Seminaire[]= [
      {
        id: 1,
        titre: "Séminaire sur les Nouvelles Technologies",
        date: new Date("2024-03-10"),
        intervenant: "Dr. Élodie Dubois (Université Aix-Marseille)",
        lieu: "Salle 301, Bâtiment B, Campus Saint-Charles, Marseille",
        resume: "Exploration des dernières avancées en intelligence artificielle",
        domaine: "Informatique"
      },
      {
        id: 2,
        titre: "Conférence sur la Médecine du Futur",
        date: new Date("2024-03-15"),
        intervenant: "Prof. Nicolas Durand (Centre Hospitalier Universitaire de la Timone)",
        lieu: "Amphithéâtre C, Faculté de Médecine, Marseille",
        resume: "Perspectives et défis de la médecine personnalisée",
        domaine: "Math"
      },
      {
        id: 3,
        titre: "Séminaire de Physique Quantique",
        date: new Date("2024-03-20"),
        intervenant: "Dr. Jeanne Leroux (Institut de Physique de Marseille)",
        lieu: "Amphithéâtre 102, Campus Luminy, Marseille",
        resume: "Compréhension des phénomènes quantiques dans les systèmes complexes",
        domaine: "Physique"
      },
      {
        id: 4,
        titre: "Colloque sur l'Écologie Urbaine",
        date: new Date("2024-03-25"),
        intervenant: "Dr. Pierre Lefèvre (Laboratoire d'Écologie Urbaine)",
        lieu: "Salle Polyvalente, Mairie du 1er arrondissement, Marseille",
        resume: "Stratégies de préservation de la biodiversité en milieu urbain",
        domaine: "Philosophie"
      },
      {
        id: 5,
        titre: "Séminaire de Littérature Comparée",
        date: new Date("2024-04-05"),
        intervenant: "Prof. Camille Martin (Université Paul Valéry, Montpellier)",
        lieu: "Salle des Actes, Bibliothèque Alcazar, Marseille",
        resume: "Analyse des influences croisées entre littératures européennes et asiatiques",
        domaine: "Philosophie"
      },
      {
        id: 6,
        titre: "Conférence sur l'Art Contemporain",
        date: new Date("2024-04-10"),
        intervenant: "Dr. Théo Roussel (Centre d'Art Contemporain, Marseille)",
        lieu: "Auditorium, Centre d'Art Contemporain, Marseille",
        resume: "Étude des tendances et des courants artistiques émergents",
        domaine: "Philosophie"
      },
      {
        id: 7,
        titre: "Séminaire sur l'Économie Sociale",
        date: new Date("2024-04-15"),
        intervenant: "Prof. Claire Dubois (Laboratoire d'Économie Sociale et Solidaire)",
        lieu: "Salle des Conférences, Maison de l'Économie Sociale et Solidaire, Marseille",
        resume: "Impact des entreprises sociales sur le développement économique local",
        domaine: "Informatique"
      },
      {
        id: 8,
        titre: "Conférence sur la Cyber-sécurité",
        date: new Date("2024-04-20"),
        intervenant: "Dr. Martin Lambert (Centre de Recherche en Sécurité Informatique)",
        lieu: "Amphithéâtre 105, Campus Saint-Jérôme, Marseille",
        resume: "Stratégies de défense contre les cybermenaces émergentes",
        domaine: "Informatique"
      },
      {
        id: 9,
        titre: "Séminaire sur la Psychologie du Travail",
        date: new Date("2024-04-25"),
        intervenant: "Dr. Sophie Girard (Laboratoire de Psychologie du Travail)",
        lieu: "Salle des Séminaires, Faculté de Psychologie, Marseille",
        resume: "Facteurs psychosociaux et performance au travail",
        domaine: "Informatique"
      },
      {
        id: 10,
        titre: "Colloque sur la Philosophie de la Science",
        date: new Date("2024-05-05"),
        intervenant: "Prof. Julien Martinez (Centre de Philosophie des Sciences)",
        lieu: "Auditorium René Char, Théâtre La Criée, Marseille",
        resume: "Débats contemporains autour de la méthodologie scientifique",
        domaine: "Informatique"
      },
      {
        id: 11,
        titre: "Séminaire sur les Mathématiques Appliquées",
        date: new Date("2024-05-10"),
        intervenant: "Dr. Jean Dupont (Institut de Mathématiques)",
        lieu: "Salle 201, Campus Saint-Charles, Marseille",
        resume: "Applications des mathématiques dans divers domaines scientifiques",
        domaine: "Math"
      },
      {
        id: 12,
        titre: "Conférence sur la Théorie des Nombres",
        date: new Date("2024-05-15"),
        intervenant: "Prof. Marie Durand (Université Aix-Marseille)",
        lieu: "Amphithéâtre A, Faculté des Sciences, Marseille",
        resume: "Avancées récentes dans la théorie des nombres",
        domaine: "Math"
      },
      {
        id: 13,
        titre: "Colloque sur l'Analyse Mathématique",
        date: new Date("2024-05-20"),
        intervenant: "Dr. Pierre Martin (Centre de Recherche Mathématique)",
        lieu: "Amphithéâtre B, Campus Luminy, Marseille",
        resume: "Exploration des concepts avancés en analyse mathématique",
        domaine: "Math"
      },
      {
        id: 14,
        titre: "Conférence sur la Mécanique Quantique",
        date: new Date("2024-05-25"),
        intervenant: "Prof. Jeanne Dubois (Institut de Physique de Marseille)",
        lieu: "Amphithéâtre C, Campus Luminy, Marseille",
        resume: "Avancées récentes dans la mécanique quantique",
        domaine: "Physique"
      },
      {
        id: 15,
        titre: "Séminaire sur l'Astrophysique Moderne",
        date: new Date("2024-06-05"),
        intervenant: "Dr. Michel Durand (Observatoire de Haute-Provence)",
        lieu: "Salle des Conférences, Observatoire de Haute-Provence, Marseille",
        resume: "Exploration des phénomènes astrophysiques contemporains",
        domaine: "Physique"
      }

    ];
  
    getAllSeminaire(): Seminaire[]{
      return this.seminaires
    }
  SeminaireById(seminaireId:number): void{
    const seminaire=this.seminaires.find(seminaire=>seminaire.id===seminaireId);
    if(!seminaire){
      throw new Error('Seminaire not found !')
    }
  }

  addSeminaire(formValue: { titre: string, date: Date, intervenant: string, lieu: string, resume: string,domaine: string }): void {
    const seminaire: Seminaire = {
      ...formValue,
      id:this.seminaires[this.seminaires.length-1].id +1,
    };
    this.seminaires.push(seminaire);
  }
 
  SupSeminaire(): void {
    if (this.seminaires.length > 0) {
      this.seminaires.pop();
      console.log('Le dernier séminaire a été supprimé avec succès.');
    } else {
      console.log('La liste des séminaires est vide.');
    }
  }
  

}


