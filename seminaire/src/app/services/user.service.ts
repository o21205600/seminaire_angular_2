import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  // Méthode pour insérer un nouvel utilisateur
  insertUser(username: string, email: string, password: string): Observable<any> {
    console.log("appel de inster user");
    const userData = { username, email, password };
    return this.http.post<any>('http://localhost:3000/inscription', userData);
  }
}